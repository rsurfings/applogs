applogs
=======

filter ELK Logs

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/applogs.svg)](https://npmjs.org/package/@rsurfings/applogs)
[![Downloads/week](https://img.shields.io/npm/dw/applogs.svg)](https://www.npmjs.com/package/@rsurfings/applogs)
[![License](https://img.shields.io/npm/l/applogs.svg)](https://github.com/rsurfings/applogs/blob/master/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g @rsurfings/applogs
$ app:logs COMMAND
running command...
$ app:logs --help [COMMAND]
USAGE
  $ app:logs COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`app:logs [URL] [USERNAME] [PASSWORD]`](#applogs-url-username-password)
* [`app:logs help [COMMAND]`](#applogs-help-command)

## `app:logs [URL] [USERNAME] [PASSWORD]`

filter ELK Logs

```
USAGE
  $ app:logs [URL] [USERNAME] [PASSWORD]

OPTIONS
  -a, --app=app                  app name
  -c, --name=name                image name
  -e, --environment=environment  (required) environment name
  -f, --force
  -h, --help                     show CLI help
  -s, --size=size                [default: 1000] result size(limit)
  -t, --id=id                    container ID
```

_See code: [src/index.ts](https://github.com/rsurfings/applogs/blob/v2.0.5/src/index.ts)_

## `app:logs help [COMMAND]`

display help for app:logs

```
USAGE
  $ app:logs help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v2.2.2/src/commands/help.ts)_
<!-- commandsstop -->